import axios from "axios";

export const fetchPaymentsBegin = () => {
	return {
		type: "FETCH_PAYMENT_BEGIN",
	};
};
export const fetchPaymentsSuccess = (payments) => {
	return {
		type: "FETCH_PAYMENT_SUCCESS",
		payload: payments,	};
};
 //abc
export const fetchPaymentsFailure = (err) => {
	return {
		type: "FETCH_PAYMENT_FAILURE",
		payload: { message: "Failed to fetch Payment.. please try again later" },
	};
};
// to be call by the components
export const fetchPayments = (payment) => {
	// returns the thunk function
	return (dispatch, getState) => {
		dispatch(fetchPaymentsBegin());
		console.log("state after fetchPaymentsBegin", getState());

		let url;
		if (payment.type !== null && payment.type !== "") {
			url = `http://localhost:8080/api/payments/findbytype/${payment.type}`;
			console.log(url);
		} else if (payment.id !== null && payment.id !== "") {
			url = `http://localhost:8080/api/payments/findbyid/${payment.id}`;
			console.log(url);
		}
		axios.get(url).then(
			(res) => {
				dispatch(fetchPaymentsSuccess(res.data));
				console.log("state after fetchPaymentsSuccess", getState());
			},
			(err) => {
				dispatch(fetchPaymentsFailure(err));
				console.log("state after fetchPaymentsFailure", getState());
			}
		);
	};
};

export const addPaymentBegin = () => {
	return {
		type: "ADD_PAYMENT_BEGIN",
	};
};

export const addPaymentSuccess = () => {
	return {
		type: "ADD_PAYMENT_SUCCESS",
		payload: { message: "Payment added successfully" },
	};
};

export const addPaymentFailure = (err) => {
	return {
		type: "ADD_PAYMENT_FAILURE",
		payload: { message: "Failed to add new Payment. please try again later" },
	};
};


export const addPayment = (payment) => {
	// returns our async thunk function
	return (dispatch, getState) => {
		return axios.post("http://localhost:8080/api/payments/save", payment).then(
			() => {
				console.log("New Payment created!");
				// this is where we can dispatch ADD_ALBUM_SUCCESS
				dispatch(addPaymentSuccess());
				console.log("state after addPaymentSuccess", getState());
			},
			(err) => {
				dispatch(addPaymentFailure(err));
				console.log("state after addPaymentFailure", getState());
			}
		);
	};
};

export const fetchServiceBegin = () => {
	return {
		type: "FETCH_SERVICE_BEGIN",
	};
};

export const fetchServiceSuccess = (status) => {
	return {
		type: "FETCH_SERVICE_SUCCESS",
		payload: status,
	};
};

export const fetchServiceFailure = (err) => {
	return {
		type: "FETCH_SERVICE_FAILURE",
		payload: { message: "Service is down.. please try again later" },
	};
};

export const fetchServiceStatus = () => {
	return (dispatch, getState) => {
		dispatch(fetchServiceBegin());
		console.log("state after fetchServiceBegin", getState());
		axios.get("http://localhost:8080/api/payments/status").then(
			(res) => {

				dispatch(fetchServiceSuccess(res.data));
				console.log("state after fetchServiceSuccess", getState());
			},
			(err) => {
				dispatch(fetchServiceFailure(err));
				console.log("state after fetchServiceFailure", getState());
			}
		);
	};
};
