import React from "react";
import PaymentId from "./PaymentId";
import PaymentType from "./PaymentType";
import PaymentAmount from "./PaymentAmount";
import PaymentDate from "./PaymentDate";
import CustomerId from "./CustomerId";

const Payment = ({ id, date, amount, type, custId }) => {
	return (
		<tr>
			<PaymentId id={id} />
			<PaymentDate date={date} />
			<PaymentAmount amount={amount} />
			<PaymentType type={type} />
			<CustomerId custId={custId} />
		</tr>
	);
};

export default Payment;
