import React from "react";

const PaymentDate = (props) => <td>{props.date}</td>;

export default PaymentDate;
