import React from "react";

const PaymentType = (props) => <td>{props.type}</td>;

export default PaymentType;
