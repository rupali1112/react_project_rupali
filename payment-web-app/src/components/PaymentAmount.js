import React from "react";

const PaymentAmount = (props) => <td>{props.amount}</td>;

export default PaymentAmount;
