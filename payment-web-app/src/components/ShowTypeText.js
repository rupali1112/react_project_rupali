import React from "react";
const ShowTypeText = ({ visible, toggle, type, setType }) => {
	return (
		visible && (
			<div className="form-group col-md-5">
				<label htmlFor="paymentType">Payment Type:</label>
				<input
					id="paymentType"
					type="text"
					className="form-control"
					placeholder="Enter Payment Type"
					value={type}
					onChange={(e) => {
						toggle(false);
						setType(e.target.value);
					}}
				/>
			</div>
		)
	);
};
export default ShowTypeText;
