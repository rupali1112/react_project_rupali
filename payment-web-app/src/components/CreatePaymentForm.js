import React, { useState } from "react";
import { addPayment } from "../actions";
import { useDispatch } from "react-redux";
import BackButton from "./BackButton";
import AddPaymentErrorOrSuccess from "./AddPaymentErrorOrSuccess";

const CreatePaymentForm = () => {
	const dispatch = useDispatch();
	const [id, setId] = useState("");
	const [type, setType] = useState("");
	const [amount, setAmount] = useState("");
	const [date, setDate] = useState("");
	const [custId, setCustId] = useState("");
	const handleSubmit = async (e) => {
		e.preventDefault();
		dispatch(
			addPayment({
				id: id,
				type: type,
				amount: amount,
				paymentDate: date,
				custId: custId,
			})
		)
			.catch(() => {
				console.log("addPayment is failed");
			})
			.finally(() => {
				console.log("addPayment thunk function is completed");
			});
	};
	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h3>Add New Payment</h3>
			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="id" style={{color: 'purple'}}>Payment ID:</label>
						<input
							id="id"
							type="number"
							className="form-control"
							placeholder="Enter Payment Id"
							required
							value={id}
							onChange={(e) => setId(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="type" style={{color: 'purple'}}>Payment Type:</label>
						<input
							id="type"
							type="text"
							className="form-control"
							placeholder="Enter Payment Type"
							required
							value={type}
							onChange={(e) => setType(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<label htmlFor="date" style={{color: 'purple'}}>Payment Date:</label>
						<input
							id="date"
							type="date"
							className="form-control"
							placeholder="Enter Payment Date"
							required
							value={date}
							onChange={(e) => setDate(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="amount" style={{color: 'purple'}}>Payment Amount:</label>
						<input
							id="amount"
							type="number"
							className="form-control"
							placeholder="Enter Payment Amount"
							required
							value={amount}
							onChange={(e) => setAmount(e.target.value)}
						/>
					</div>
					<div className="form-group col-md-5">
						<label htmlFor="custId" style={{color: 'purple'}}>Customer ID:</label>
						<input
							id="custId"
							type="number"
							className="form-control"
							placeholder="Enter Customer Id"
							required
							value={custId}
							onChange={(e) => setCustId(e.target.value)}
						/>
					</div>
				</div>
				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Create Payment"
							className="btn btn-outline-secondary"
						/>
					</div>
					<BackButton />
					<AddPaymentErrorOrSuccess />
				</div>
			</form>
		</div>
	);
};

export default CreatePaymentForm;
