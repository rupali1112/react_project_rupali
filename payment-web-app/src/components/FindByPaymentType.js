import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { fetchPayments } from "../actions";
import PaymentListing from "./PaymentListing";
import ShowIdText from "./ShowIdText";
import ShowTypeText from "./ShowTypeText";
import BackButton from "./BackButton";

const FindByPaymentType = () => {
	const dispatch = useDispatch();
	const [type, setType] = useState("");
	const [id, setId] = useState("");
	const [visibleId, setVisibilityId] = useState(true);
	const [visibleType, setVisibilityType] = useState(true);
	const handleSubmit = (e) => {
		e.preventDefault();
		dispatch(fetchPayments({ type: type, id: id }));
	};
	return (
		<div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
			<h5>Find Payment</h5>
			<form onSubmit={handleSubmit} autoComplete="off">
				<div className="form-row">
					<ShowTypeText
						type={type}
						setType={setType}
						toggle={setVisibilityId}
						visible={visibleType}
					></ShowTypeText>

					<ShowIdText
						id={id}
						setId={setId}
						toggle={setVisibilityType}
						visible={visibleId}
					/>
				</div>

				<div className="form-row">
					<div className="form-group col-md-5">
						<input
							type="submit"
							value="Search"
							className="btn btn-outline-secondary"
						/>
					</div>
					<BackButton />
				</div>
				<PaymentListing type={type} id={id} />
			</form>
		</div>
	);
};

export default FindByPaymentType;
