import React, { useState, useEffect } from "react";
import PaymentHeader from "./PaymentHeader";
import PaymentTableBody from "./PaymentTableBody";
import PaymentBody from "./PaymentBody";
import { useSelector } from "react-redux";

const PaymentListing = () => {
	const [loadResult, setLoadResult] = useState(null);
	const [loadError, setLoadError] = useState(null);
	const payments = useSelector((state) => state.payment.entities);
	const error = useSelector((state) => state.payment.error);
	const loading = useSelector((state) => state.payment.loading);

	useEffect(() => {
		return () => {
			setLoadResult(true);
		};
	}, [payments]);
	useEffect(() => {
		return () => {
			setLoadError(true);
		};
	}, [error]);

	let loadList = false;

	if (payments && Array.isArray(payments)) {
		loadList = true;
	}

	if (loadError && error) {
		console.log("after useEffect loadError", loadError);
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}

	if (loading) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}

	if (loadResult) {
		console.log("after useEffect loadResult", loadResult);
		return (
			<table className="table table-hover">
				<PaymentHeader />
				<PaymentTableBody payments={payments} loadList={loadList} />
				<PaymentBody payments={payments} loadList={loadList} />
			</table>
		);
	}

	return <div></div>;
};

export default PaymentListing;
