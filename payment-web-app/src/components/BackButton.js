import { useHistory } from "react-router-dom";
import React from "react";

const BackButton = ({ payments, loadList }) => {
	const history = useHistory();
	const handleButtonBack = (e) => {
		history.push("/");
	};
	return (
		<div className="form-group col-md-5">
			<input
				type="button"
				value="Back"
				className="btn btn-outline-secondary"
				onClick={handleButtonBack}
			/>
		</div>
	);
};
export default BackButton;
