import React from "react";

const About = () => {
	return (
		<div>
			<h3 style={{color: 'tomato'}}>About Us</h3>
			<p style={{color: 'purple'}}>
				Payment application is used to add the payment details and also we can
				fetch the payment details. Fetch can be done in 2 ways. Find by Id and
				Find by payment type.
			</p>
		</div>
	);
};
export default About;
