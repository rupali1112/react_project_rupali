import Banner from "./Banner";
import Navbar from "./Navbar";
import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

import CreatePaymentForm from "./CreatePaymentForm";
import FindStatus from "./FindStatus";
import FindByPaymentType from "./FindByPaymentType";
import About from "./About";

const App = () => {
	return (
		<Router>
			<Navbar />
			<Switch>
				<Route exact path="/">
					<Banner />
					<About />
					<FindStatus />
				</Route>
				<Route path="/add">
					<CreatePaymentForm />
				</Route>
				<Route path="/find">
					<FindByPaymentType />
				</Route>
			</Switch>
		</Router>
	);
};

export default App;
