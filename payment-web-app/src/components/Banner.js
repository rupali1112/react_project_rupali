import React from "react";

const Banner = () => (
	<section className="jumbotron text-center">
		<div className="container">
			<h1 className="jumbotron-heading">Payment Application</h1>
			<p className="lead text-muted">
				Payment Application is used to add the Payment and to find the payment.
			</p>
		</div>
	</section>
);

export default Banner;
