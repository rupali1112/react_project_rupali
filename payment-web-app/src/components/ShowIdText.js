import React from "react";
const ShowIdText = ({ visible, toggle, id, setId }) => {
	return (
		visible && (
			<div className="form-group col-md-5">
				<label htmlFor="paymentId">OR Payment ID:</label>
				<input
					id="paymentId"
					type="number"
					className="form-control"
					placeholder="Enter Payment ID"
					value={id}
					onChange={(e) => {
						toggle(false);
						setId(e.target.value);
					}}
				/>
			</div>
		)
	);
};
export default ShowIdText;
