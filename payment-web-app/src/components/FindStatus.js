import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { fetchServiceStatus } from "../actions";
import { useDispatch } from "react-redux";

const FindStatus = () => {
	//const [showMessage, setShowMessage] = useState(null);
	const success = useSelector((state) => state.status.data);
	const error = useSelector((state) => state.status.error);
	const loading = useSelector((state) => state.status.loading);
	const dispatch = useDispatch();
	useEffect(() => {
		console.log("inside useeffect of FindStatus");
		dispatch(fetchServiceStatus());
	}, []);

	if (loading) {
		return (
			<div>
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}
	if (error) {
		//console.log("inside error block", showMessage);
		return (
			<div>
				<b>Service Status:{error.message}</b>
			</div>
		);
	}
	if (success) {
		//console.log("inside success block", showMessage);
		return (
			<div>
				<b>Service Status: {success}</b>
			</div>
		);
	}

	return <div></div>;
};
export default FindStatus;
