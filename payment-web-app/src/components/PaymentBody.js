import Payment from "./Payment";
import React from "react";
const PaymentBody = ({ payments, loadList }) => {
	return (
		!loadList && (
			<tbody>
				<Payment
					key={payments.id}
					id={payments.id}
					date={payments.paymentDate}
					amount={payments.amount}
					type={payments.type}
					custId={payments.custId}
				/>
			</tbody>
		)
	);
};
export default PaymentBody;
