import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

const AddPaymentErrorOrSuccess = () => {
	const [showMessage, setShowMessage] = useState(null);
	const error = useSelector((state) => state.addPayment.error);
	const success = useSelector((state) => state.addPayment.success);
	useEffect(() => {
		return () => {
			setShowMessage(true);
		};
	}, [error, success]);
	if (error && showMessage) {
		console.log("inside erro block", showMessage);
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}
	if (success && showMessage) {
		console.log("inside success block", showMessage);
		return (
			<div className="d-flex justify-content-center">{success.message}</div>
		);
	}
	return null;
};
export default AddPaymentErrorOrSuccess;
