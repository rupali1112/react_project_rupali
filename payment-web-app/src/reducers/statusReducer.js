const initialState = {};

const statusReducer = (state = initialState, action) => {
	console.log(`received ${action.type} dispatch in StatusReducer`);
	switch (action.type) {
		case "FETCH_SERVICE_BEGIN":
			return { ...state, loading: true, error: null };
		case "FETCH_SERVICE_SUCCESS":
			return { ...state, data: action.payload, loading: false };
		case "FETCH_SERVICE_FAILURE":
			return { ...state, data: " ", loading: false, error: action.payload };
		default:
			return state;
	}
};
export default statusReducer;
