const errorReducer = (state = [], action) => {
	console.log(`received ${action.type} dispatch in errorsReducer`);
	switch (action.type) {
		case "FETCH_PAYMENT_FAILURE":
			return [...state, action.payload];
		case "ADD_PAYMENT_FAILURE":
			return [...state, action.payload];
		default:
			return state;
	}
};

export default errorReducer;
